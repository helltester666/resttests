﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Text;
using NUnit.Framework.Constraints;
using RestSharp;
using RestTest2.Models;

namespace RestTest2.Utilities
{
    public class ApiRequest<T> where T : new()
    {
        private readonly RestClient _client;
        private readonly RestRequest _request;
        private IRestResponse<T> _response;

        public ApiRequest(string baseUrl)
        {
            _client = new RestClient(baseUrl);
            _request = new RestRequest();
        }

        public void SetEndpoint(string url)
        {
            _request.Resource = url;
        }

        public void SetMethod(Method method)
        {
            _request.Method = method;
        }

        public void SetBody(object body)
        {
            _request.AddJsonBody(body);
        }

        public void SetUrlParam(string name, string value)
        {
            _request.AddUrlSegment(name, value);
        }

        public void SetHeader(string name, string value)
        {
            _request.AddHeader(name, value);
        }

        public void SetQueryParam(string name, string value)
        {
            _request.AddQueryParameter(name, value);
        }

        public void Execute()
        {
            _response = _client.Execute<T>(_request);
        }

        public T GetResponse()
        {
            return _response.Data;
        }

        public bool IsSuccessful()
        {
            return _response.IsSuccessful;
        }

        public HttpStatusCode GetStatusCode()
        {
            return _response.StatusCode;
        }
    }
}
