using System.Collections.Generic;
using System.Linq;
using System.Net;
using NUnit.Framework;
using RestSharp;
using RestTest2;
using RestTest2.Models;
using RestTest2.Utilities;

namespace Tests
{
    public class Tests : TestBase
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void TestGet_WhenSendTestresults_ShouldReturnOk()
        {
            var _request = new ApiRequest<List<TestResult>>(BaseUrl.AbsoluteUri);
            _request.SetEndpoint("testresults");

            _request.SetMethod(Method.GET);
            _request.Execute();

            Assert.AreEqual(true, _request.IsSuccessful());
            Assert.AreNotEqual(0, _request.GetResponse().Count);
        }
        
        [Test]
        public void TestPost_WhenSendTestresults_ShouldCreateNewResult()
        {
            var _request = new ApiRequest<TestResult>(BaseUrl.AbsoluteUri);
            _request.SetEndpoint("testresults");

            var newTestResult = new TestResultPostRequestDto
            {
                Name = "Test name"
            };

            _request.SetMethod(Method.POST);
            _request.SetBody(newTestResult);

            _request.Execute();

            Assert.AreEqual(true, _request.IsSuccessful());

            var request = new ApiRequest<List<TestResult>>(BaseUrl.AbsoluteUri);
            request.SetEndpoint("testresults");

            request.SetMethod(Method.GET);

            request.Execute();
            
            Assert.That(request.GetResponse().Any(x => x.Id == _request.GetResponse().Id));
            Assert.That(request.GetResponse().Any(x => x.Name == _request.GetResponse().Name));
        }
        
        [Test]
        public void TestPut_WhenSendTestresults_ShouldRefreshWholeResult()
        {
            var _request = new ApiRequest<TestResult>(BaseUrl.AbsoluteUri);
            _request.SetEndpoint("testresults");

            var newTestResult = new TestResultPutRequestDto
            {
                Name = "Test name",
                RandomField = "Random test field"
            };

            _request.SetMethod(Method.POST);
            _request.SetBody(newTestResult);

            _request.Execute();

            Assert.AreEqual(true, _request.IsSuccessful());

            var request = new ApiRequest<TestResultPutRequestDto>(BaseUrl.AbsoluteUri);
            request.SetEndpoint("testresults/{id}");

            request.SetUrlParam("id", _request.GetResponse().Id.ToString());
            request.SetMethod(Method.PUT);
            request.SetBody(new TestResult
            {
                Name = "Edited name"
            });

            request.Execute();

            Assert.That(request.GetResponse().Name, Is.EqualTo("Edited name"));
            Assert.That(request.GetResponse().RandomField, Is.Null);
        }
        
        [Test]
        public void TestPatch_WhenSendTestresults_ShouldRefreshOnlyExactFields()
        {
            var newTestResult = new TestResultPutRequestDto
            {
                Name = "Test name",
                RandomField = "Random test field"
            };

            var _request = new ApiRequest<TestResult>(BaseUrl.AbsoluteUri);
            _request.SetEndpoint("testresults");

            _request.SetMethod(Method.POST);
            _request.SetBody(newTestResult);

            _request.Execute();

            Assert.AreEqual(true, _request.IsSuccessful());

            var request = new ApiRequest<TestResultPutRequestDto>(BaseUrl.AbsoluteUri);
            request.SetEndpoint("testresults/{id}");
            request.SetUrlParam("id", _request.GetResponse().Id.ToString());
            request.SetMethod(Method.PATCH);
            request.SetBody(new TestResult
            {
                Name = "Edited name"
            });

            request.Execute();
            
            Assert.That(request.GetResponse().Name, Is.EqualTo("Edited name"));
            Assert.That(request.GetResponse().RandomField, Is.EqualTo("Random test field"));
        }
        
        [Test]
        public void TestDelete_WhenSendTestresults_ShouldDeleteExisting()
        {
            var newTestResult = new TestResultPostRequestDto
            {
                Name = "Test name"
            };

            var _request = new ApiRequest<TestResult>(BaseUrl.AbsoluteUri);
            _request.SetEndpoint("testresults");

            _request.SetMethod(Method.POST);
            _request.SetBody(newTestResult);

            _request.Execute();

            Assert.AreEqual(true, _request.IsSuccessful());

            var request = new ApiRequest<TestResult>(BaseUrl.AbsoluteUri);
            request.SetEndpoint("testresults/{id}");

            request.SetUrlParam("id", _request.GetResponse().Id.ToString());
            request.SetMethod(Method.DELETE);

            request.Execute();

            Assert.AreEqual(true, request.IsSuccessful());

            var requestGet = new ApiRequest<TestResult>(BaseUrl.AbsoluteUri);
            requestGet.SetEndpoint("testresults/{id}");

            requestGet.SetMethod(Method.GET);
            requestGet.Execute();

            Assert.That(requestGet.IsSuccessful(), Is.False);
            Assert.That(requestGet.GetStatusCode(), Is.EqualTo(HttpStatusCode.NotFound));
        }

        [Test]
        public void TestGet_WhenSendTestresultsWithHeader_ShouldReturnOk()
        {
            var _request = new ApiRequest<List<TestResult>>(BaseUrl.AbsoluteUri);
            _request.SetEndpoint("testresults");

            _request.SetMethod(Method.GET);
            _request.SetHeader("Authorization", "Basic Some string");

            _request.Execute();
            
            Assert.AreEqual(true, _request.IsSuccessful());
        }
        
        [Test]
        public void TestGet_WhenSendTestresultsWithQueryParam_ShouldReturnFilteredList()
        {
            var _request = new ApiRequest<List<TestResult>>(BaseUrl.AbsoluteUri);
            _request.SetEndpoint("testresults");

            _request.SetMethod(Method.GET);
            _request.SetQueryParam("id", 0.ToString());
            _request.Execute();
            
            Assert.AreEqual(true, _request.IsSuccessful());
            Assert.That(_request.GetResponse().Count, Is.EqualTo(0));
        }
    }
}