﻿namespace RestTest2
{
    public class Settings
    {
        public string Protocol { get; set; }
        public string Url { get; set; }
        public int Port { get; set; }
    }
}
