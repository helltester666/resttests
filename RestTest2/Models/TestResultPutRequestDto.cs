﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RestTest2.Models
{
    public class TestResultPutRequestDto
    {
        public string Name { get; set; }
        public string RandomField { get; set; }
    }
}
