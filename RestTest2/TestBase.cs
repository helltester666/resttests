﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using Newtonsoft.Json;
using NUnit.Framework;

namespace RestTest2
{
    [TestFixture]
    public class TestBase
    {
        protected Settings Settings;
        public Uri BaseUrl;

        [OneTimeSetUp]
        public void SetSettings()
        {
            var json = File.ReadAllText("config.json");
            Settings = JsonConvert.DeserializeObject<Settings>(json);

            BaseUrl = new Uri($"{Settings.Protocol}://{Settings.Url}:{Settings.Port}");
        }
    }
}
